package com.eeeffff.yapidoc.maven;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import com.eeeffff.yapidoc.maven.yapi.upload.UploadToYapi;
import com.eeeffff.yapidoc.models.OpenAPI;
import com.eeeffff.yapidoc.utils.Json;

public class UploadTest {
	@Test
	public void testUpload() throws Exception {
		String filePath = "/tmp/yapidoc.json";
		String yapiProjectToken = "5f1f61e5eeb5a0021ed55394fd536011ad9b6e45f871556c95f88a68ebe3a98b";
		String yapiUrl="http://yapi.imlaidian.com/";
		String json = FileUtils.readFileToString(new File(filePath), "utf-8");
		OpenAPI openAPI = Json.mapper().readValue(json, OpenAPI.class);
		UploadToYapi uploadToYapi = new UploadToYapi(yapiProjectToken, yapiUrl);
		uploadToYapi.upload(openAPI, true);
	}
}
